docker-build:
	@docker-compose build php

docker-pull:
	@docker-compose pull

docker-push:
	@docker-compose push php

start: 
	@docker-compose up -d php

shell: start
	@docker-compose exec php zsh
