# Domain models in practice

This is a fork of the original [Cinemarcos Quality Reservation System - A prototypical implementation](https://gitlab.com/heimeshoff/domain-models-in-practice) by [@heimeshoff](https://gitlab.com/heimeshoff).

This fork ports everything to PHP.

## Usage
This fork leverages `make` + Docker + Docker Compose to run in PHP 8.
To start the project, simply do `make` or `make run`: it will try to build or pull the Docker image, and the start the project by installing all the Composer dependencies and running all the checks (code style, static analysis, tests).
