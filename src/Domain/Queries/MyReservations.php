<?php

declare(strict_types=1);

namespace DDD\Domain\Queries;

use DDD\Domain\ValueTypes\Customer;

class MyReservations
{
    public function __construct(
        public Customer $customer,
    ) {
    }
}
