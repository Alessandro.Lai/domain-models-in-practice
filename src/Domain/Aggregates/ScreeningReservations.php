<?php

declare(strict_types=1);

namespace DDD\Domain\Aggregates;

use DDD\Domain\Events\Event;
use DDD\Domain\Events\SeatCannotBeReserved;
use DDD\Domain\Events\SeatHasBeenReserved;
use DDD\Domain\ValueTypes\Cinema;
use DDD\Domain\ValueTypes\Customer;
use DDD\Domain\ValueTypes\Screening;
use DDD\Domain\ValueTypes\Seat;

class ScreeningReservations
{
    public function __construct(
        private ScreeningReservationsState $screeningReservationsState
    ) {
    }

    /**
     * @param callable(Event):void $publish
     */
    public function reserve(callable $publish, Screening $screening, Seat $seat, Customer $customer): void
    {
        if ($this->seatAlreadyReserved($screening, $seat)) {
            $publish(new SeatCannotBeReserved($screening, $seat));
        } else {
            $publish(new SeatHasBeenReserved($screening, $seat, $customer));
        }
    }

    private function seatAlreadyReserved(Screening $screening, Seat $seatToSearch): bool
    {
        foreach ($this->screeningReservationsState->getSeats($screening) as $seat) {
            if ($seat->equals($seatToSearch)) {
                return true;
            }
        }

        return false;
    }
}
