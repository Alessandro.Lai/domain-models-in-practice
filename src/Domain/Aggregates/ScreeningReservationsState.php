<?php

declare(strict_types=1);

namespace DDD\Domain\Aggregates;

use DDD\Domain\Events\Event;
use DDD\Domain\Events\ScreeningHasBeenPlanned;
use DDD\Domain\Events\SeatHasBeenReserved;
use DDD\Domain\ValueTypes\Screening;
use DDD\Domain\ValueTypes\Seat;

class ScreeningReservationsState
{
    // The Aggregate that protects the invariants around reserving seats
    // Ensures transactional consistent and domain-correct behaviour for all involved classes
    // In a real life implementation you wouldn't store this in an Aggregate.cs,
    // but put it in domain specific files and folders.

    /** @var array<string, Seat[]> List of seats by Screenings */
    private array $screenings = [];

    public function __construct(Event ...$events)
    {
        foreach ($events as $event) {
            $this->apply($event);
        }
    }

    private function apply(Event $event): void
    {
        if ($event instanceof ScreeningHasBeenPlanned) {
            $screening = $event->screening;
            $this->screenings[$screening->toString()] = [];
        } elseif ($event instanceof SeatHasBeenReserved) {
            $this->screenings[$event->screening->toString()][] = $event->seat;
        }
    }

    /**
     * @return Seat[]
     */
    public function getSeats(Screening $screening): array
    {
        return $this->screenings[$screening->toString()];
    }
}
