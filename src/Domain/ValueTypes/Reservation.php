<?php

declare(strict_types=1);

namespace DDD\Domain\ValueTypes;

class Reservation
{
    public function __construct(
        public Screening $screening,
        public Seat $seat,
    ) {
    }
}
