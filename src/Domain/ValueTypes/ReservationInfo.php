<?php

declare(strict_types=1);

namespace DDD\Domain\ValueTypes;

class ReservationInfo
{
    /** @var Reservation[] */
    private array $reservations;

    /**
     * @param Reservation ...$reservations
     */
    public function __construct(Reservation ...$reservations)
    {
        $this->reservations = $reservations;
    }

    /**
     * @return Reservation[]
     */
    public function getReservations(): array
    {
        return $this->reservations;
    }

    public function equals(mixed $other): bool
    {
        if (! $other instanceof self) {
            return false;
        }

        if (count($this->reservations) !== count($other->reservations)) {
            return false;
        }

        foreach ($this->reservations as $reservation) {
            if (! in_array($reservation, $other->reservations, true)) {
                return false;
            }
        }

        return true;
    }

    public function getHashCode(): string
    {
        if (empty($this->reservations)) {
            return '0';
        }

        return md5(serialize($this->reservations));
    }
}
