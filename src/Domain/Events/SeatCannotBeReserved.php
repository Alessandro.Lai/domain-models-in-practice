<?php

declare(strict_types=1);

namespace DDD\Domain\Events;

use DDD\Domain\ValueTypes\Screening;
use DDD\Domain\ValueTypes\Seat;

class SeatCannotBeReserved implements Event
{
    public function __construct(
        public Screening $screening,
        public Seat $seat,
    ) {
    }
}
