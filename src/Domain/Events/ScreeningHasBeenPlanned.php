<?php

declare(strict_types=1);

namespace DDD\Domain\Events;

use DDD\Domain\ValueTypes\Cinema;
use DDD\Domain\ValueTypes\Screening;

class ScreeningHasBeenPlanned implements Event
{
    public function __construct(
        public Screening $screening,
        public \DateTimeInterface $date,
        public Cinema $cinema,
    ) {
    }
}
