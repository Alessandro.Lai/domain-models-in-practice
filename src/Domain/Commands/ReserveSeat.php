<?php

declare(strict_types=1);

namespace DDD\Domain\Commands;

use DDD\Domain\ValueTypes\Customer;
use DDD\Domain\ValueTypes\Screening;
use DDD\Domain\ValueTypes\Seat;

class ReserveSeat
{
    public function __construct(
        public Screening $screening,
        public Seat $seat,
        public Customer $customer,
    ) {
    }
}
