<?php

declare(strict_types=1);

namespace DDD\Domain\ReadModels;

use DDD\Domain\Events\Event;
use DDD\Domain\Events\SeatHasBeenReserved;
use DDD\Domain\ValueTypes\Customer;
use DDD\Domain\ValueTypes\Reservation;

class CustomerReservations
{
    /** @var array<string, Reservation[]> */
    private array $reservations = [];

    public function __construct(Event ...$events)
    {
        foreach ($events as $event) {
            $this->apply($event);
        }
    }

    private function apply(Event $event): void
    {
        if (! $event instanceof SeatHasBeenReserved) {
            return;
        }

        $this->reservations[$event->customer->toString()] ??= [];
        $this->reservations[$event->customer->toString()][] = new Reservation($event->screening, $event->seat);
    }

    /**
     * @return Reservation[]
     */
    public function getFor(Customer $customer): array
    {
        return $this->reservations[$customer->toString()];
    }
}
