<?php

declare(strict_types=1);

namespace DDD\Infrastructure;

use DDD\Domain\Aggregates\ScreeningReservations;
use DDD\Domain\Aggregates\ScreeningReservationsState;
use DDD\Domain\Commands\ReserveSeat;
use DDD\Domain\Events\Event;

class CommandHandler
{
    // A very simple commandhandler that is not yet optimised for real life usage.
    // Use it as a starting point. Depending on your needs you could add:
    //   Real routing, pluggable logging/auth/etc.,
    //   Chain of responsibility pattern through parent class... or the opposite
    //   go all in on functional programming.
    // Whatever you do, the basic pattern of separating infrastructure from domain
    // starts here.

    /** @var Event[] */
    protected array $history;

    public function __construct(Event ...$history)
    {
        $this->history = $history;
    }

    /**
     * @param callable(Event): void $publish
     */
    public function handle(object $command, callable $publish): void
    {
        if ($command instanceof ReserveSeat) {
            $state = new ScreeningReservationsState(...$this->history);
            $screeningReservations = new ScreeningReservations($state);
            $screeningReservations->reserve($publish, $command->screening, $command->seat, $command->customer);

            return;
        }

        throw new \InvalidArgumentException('No handler found for ' . get_class($command));
    }
}
