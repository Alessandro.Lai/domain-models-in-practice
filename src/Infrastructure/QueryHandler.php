<?php

declare(strict_types=1);

namespace DDD\Infrastructure;

use DDD\Domain\Events\Event;
use DDD\Domain\Queries\MyReservations;
use DDD\Domain\ReadModels\CustomerReservations;
use DDD\Domain\ValueTypes\ReservationInfo;

class QueryHandler
{
    // This Queryhandler instantiates all readmodels only once, and never updates them live
    // This behaviour will follow in the coming days

    /** @var Event[] */
    private array $history;

    public function __construct(
        Event ...$history,
    ) {
        $this->history = $history;
    }

    /**
     * @param callable(object): void $respond
     */
    public function handle(object $query, callable $respond): void
    {
        if ($query instanceof MyReservations) {
            $readModel = new CustomerReservations(...$this->history);
            $respond(new ReservationInfo(...$readModel->getFor($query->customer)));
            // Did I just forget to check if the customer actually exist?
            // Well, there would never be a query with an illegal customer, right? :D:D:D

            return;
        }

        throw new \InvalidArgumentException('No handler found for ' . get_class($query));
    }
}
