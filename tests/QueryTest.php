<?php

declare(strict_types=1);

namespace DDD\Tests;

use DDD\Domain\ValueTypes\Reservation;

class QueryTest extends BaseTestCase
{
    // These are domain-driven tests about the query side of your system
    // Ideally only written in Given / When_queried / Then_expect_result using Events and Queries/responses
    public function testAFreeSeatCanBeReserved(): void
    {
        $this->given(
            $this->screeningHasBeenPlanned($this->screening1(), $this->december2nd2020(), $this->cinema1()),
            $this->seatHasBeenReserved($this->screening1(), $this->seatA1(), $this->Marco()),
            $this->seatHasBeenReserved($this->screening1(), $this->seatA2(), $this->Marco()),
        );

        $this->whenQuery(
            $this->myReservations($this->Marco())
        );

        $this->thenExpectResponse(
            $this->reservations(
                new Reservation($this->screening1(), $this->seatA1()),
                new Reservation($this->screening1(), $this->seatA2()),
            )
        );
    }
}
