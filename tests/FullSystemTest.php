<?php

declare(strict_types=1);

namespace DDD\Tests;

use DDD\Domain\ValueTypes\Reservation;

class FullSystemTest extends BaseTestCase
{
    // These are domain-driven tests that test the interoperation of your command and query side
    // Because the Events don't matter from the outside, we do not have to test for them
    public function testACustomerCanReserveAdditionalSeatsAndSeeTheirFullReservations(): void
    {
        $this->given(
            $this->screeningHasBeenPlanned($this->screening1(), $this->december2nd2020(), $this->cinema1()),
            $this->seatHasBeenReserved($this->screening1(), $this->seatA1(), $this->Marco()),
            $this->seatHasBeenReserved($this->screening1(), $this->seatA3(), $this->Tina()),
        );

        $this->when(
            $this->reserveSeat($this->screening1(), $this->seatA2(), $this->Marco())
        );

        $this->whenQuery(
            $this->myReservations($this->Marco())
        );

        $this->thenExpectResponse(
            $this->reservations(
                new Reservation($this->screening1(), $this->seatA1()),
                new Reservation($this->screening1(), $this->seatA2()),
            )
        );
    }
}
