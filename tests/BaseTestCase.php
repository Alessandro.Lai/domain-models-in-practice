<?php

declare(strict_types=1);

namespace DDD\Tests;

use DDD\Domain\Commands\ReserveSeat;
use DDD\Domain\Events\Event;
use DDD\Domain\Events\ScreeningHasBeenPlanned;
use DDD\Domain\Events\SeatCannotBeReserved;
use DDD\Domain\Events\SeatHasBeenReserved;
use DDD\Domain\Queries\MyReservations;
use DDD\Domain\ReadModels\CustomerReservations;
use DDD\Domain\ValueTypes\Cinema;
use DDD\Domain\ValueTypes\Customer;
use DDD\Domain\ValueTypes\Reservation;
use DDD\Domain\ValueTypes\ReservationInfo;
use DDD\Domain\ValueTypes\Screening;
use DDD\Domain\ValueTypes\Seat;
use DDD\Infrastructure\CommandHandler;
use DDD\Infrastructure\QueryHandler;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;

abstract class BaseTestCase extends TestCase
{
    /** @var Event[] */
    private array $history = [];
    /** @var Event[] */
    private array $publishedEvents = [];
    private ?object $queriedResponse = null;
    private ?CustomerReservations $readModel = null;

    protected function setUp(): void
    {
        parent::setUp();

        $this->history = [];
        $this->publishedEvents = [];
        $this->queriedResponse = null;
    }

    protected function given(Event ...$events): void
    {
        array_push($this->history, ...$events);
        $this->readModel = new CustomerReservations(...$this->history);
    }

    protected function when(object $command): void
    {
        $handler = new CommandHandler(...$this->history);
        $infra = $this;
        $handler->handle(
            $command,
            function (Event $newEvent) use ($infra): void {
                $infra->publishedEvents[] = $newEvent;
            }
        );
    }

    protected function whenQuery(object $query): void
    {
        $handler = new QueryHandler(...$this->history, ...$this->publishedEvents);
        $infra = $this;
        $handler->handle($query, function (object $response) use ($infra): void {
            $infra->queriedResponse = $response;
        });
    }

    protected function thenExpect(Event ...$expectedEvents): void
    {
        $this->assertEquals($expectedEvents, $this->publishedEvents, 'Published events do not match');
    }

    protected function thenExpectResponse(object $expectedResponse): void
    {
        $this->assertEquals($expectedResponse, $this->queriedResponse, 'Expected response do not match');
    }

    // Factory methods for commands, queries and alike.
    // Easier to read (since the "new" keyword is omitted, and also decouple tests from the actual implementation.
    protected function reserveSeat(Screening $screening, Seat $seat, Customer $customer): ReserveSeat
    {
        return new ReserveSeat($screening, $seat, $customer);
    }

    protected function seatHasBeenReserved(Screening $screening, Seat $seat, Customer $customer): SeatHasBeenReserved
    {
        return new SeatHasBeenReserved($screening, $seat, $customer);
    }

    protected function screeningHasBeenPlanned(Screening $screening, \DateTimeInterface $date, Cinema $cinema): ScreeningHasBeenPlanned
    {
        return new ScreeningHasBeenPlanned($screening, $date, $cinema);
    }

    protected function seatCannotBeReserved(Screening $screening, Seat $seat): SeatCannotBeReserved
    {
        return new SeatCannotBeReserved($screening, $seat);
    }

    protected function myReservations(Customer $customer): MyReservations
    {
        return new MyReservations($customer);
    }

    protected function reservations(Reservation ...$reservations): ReservationInfo
    {
        return new ReservationInfo(...$reservations);
    }

    protected function reservation(Screening $screening, Seat $seat): Reservation
    {
        return new Reservation($screening, $seat);
    }

//
    // Any Value that is needed to describe a test, but that has no respective ValueType in the domain.
    // Keeping your test clean and easier to write through exploration with intellisense
    protected function december2nd2020(): \DateTimeInterface
    {
        return new \Safe\DateTime('2020-12-02 00:00:00');
    }

    // These are Semantic UUID Identifiers for various things in the domain.
    // It is easier to reason about "Marco" in a test than about "99A83F47..." ;)
    // Pro-Tip: Of course you could use the value type "CustomerReference" instead of a Guid!
    protected function Marco(): Customer
    {
        return new Customer('99A83F47-32D2-4007-A5B8-87C2F1BFD197'); // Reference to the Customer Marco H.
    }

    protected function Tina(): Customer
    {
        return new Customer('BC6BF2B8-A198-4877-980C-E454A4E42628'); // Reference to the Customer Tina F.
    }

    protected function cinema1(): Cinema
    {
        return new Cinema('EC15243D-58D5-470D-922D-730FAAFB1B36'); // Reference to $cinema 1 in the first floor
    }

    protected function seatA1(): Seat
    {
        return new Seat('108F0340-C954-44E3-84C2-6CD453351553'); // Reference to the first seat in row A in $cinema 1
    }

    protected function seatA2(): Seat
    {
        return new Seat('4E5DAC6B-E898-4653-91C6-941204E64BFC'); // Reference to the second seat in row A in $cinema 1
    }

    protected function seatA3(): Seat
    {
        return new Seat('FF87BBFF-6C84-4DD8-A658-9FCE5D94CF1B'); // Reference to the third seat in row A in $cinema 1
    }

    protected function screening1(): Screening
    {
        return new Screening('42F573B3-257B-48C3-BC94-7ACC37C5D3F4'); // Reference to the screening of "Avengers End Game" on december the second.}
    }
}
