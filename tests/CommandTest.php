<?php

declare(strict_types=1);

namespace DDD\Tests;

class CommandTest extends BaseTestCase
{
    public function testAFreeSeatCanBeReserved(): void
    {
        $this->given(
            $this->screeningHasBeenPlanned($this->screening1(), $this->december2nd2020(), $this->cinema1()),
            $this->seatHasBeenReserved($this->screening1(), $this->seatA1(), $this->Tina()),
        );

        $this->when(
            $this->reserveSeat($this->screening1(), $this->seatA2(), $this->Marco()),
        );

        $this->thenExpect(
            $this->seatHasBeenReserved($this->screening1(), $this->seatA2(), $this->Marco()),
        );
    }

    public function testAnAlreadyReservedSeatCannotBeReserved(): void
    {
        $this->given(
            $this->screeningHasBeenPlanned($this->screening1(), $this->december2nd2020(), $this->cinema1()),
            $this->seatHasBeenReserved($this->screening1(), $this->seatA1(), $this->Tina()),
        );

        $this->when(
            $this->reserveSeat($this->screening1(), $this->seatA1(), $this->Marco()),
        );

        $this->thenExpect(
            $this->seatCannotBeReserved($this->screening1(), $this->seatA1())
        );
    }
}
